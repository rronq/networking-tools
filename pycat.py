import sys
import socket
import getopt
import threading
import subprocess
import argparse

#global vars
listen = False
command = False
upload = None
execute = None
target = None
uploadDst = None
port = None


def clientSender(buffer):
    print("Sending data to client on port " + str(port))
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    try:
        client.connect((target,port))
        
        if len(buffer):
            client.send(buffer.encode())
            
        while True:
            #wait for data back
            recv_len = 1
            response = ""
            
            while recv_len:
                print("Waiting for response from client...")
                data=client.recv(4096)
                recv_len = len(data)
                response+=data.decode(errors="ignore")
                
                if recv_len < 4096:
                    break
            print(response, end="")
            #wait for input
            buffer = input("")
            buffer += "\n"
            
            #send it off
            client.send(buffer.encode())
            
    except:
        print("[*] Exception! Exiting.")
        
        #close connection
    finally:
        client.close()

def serverLoop():
    global target
    print("Entering server loop...")
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((target,port))
    server.listen(5)
    
    while True:
        clientSocket, addr = server.accept()
        print("[*]Accepted connection from: %s:%d" %(addr[0],addr[1]))
        #spin off a thread to handle new client
        clientThread = threading.Thread(target=clientHandler, args=(clientSocket,))
        clientThread.start()
        
def runCommand(command):

    command = command.rstrip()
    print("Executing command:" + command)
    #run command and get output back
    try:
        output=subprocess.check_output(command,stderr=subprocess.STDOUT,shell=True)
    except:
        output="Failed to execute command.\r\n"
    #send output back to client
    return output

def clientHandler(clientSocket):
    global upload
    global execute
    global command
    print("Handling client socket...")
    
    #check for upload
    if uploadDst is not None:
        print("Entering file upload...")
        #read in all bytes and write to destination
        fileBuffer = ""
        #keep reading data until none is available
        while True:
            data = clientSocket.recv(1024)
            if not data:
                break
            else:
                fileBuffer += data.decode()
                
        #take bytes and write them output
        try:
            fileDescriptor = open(uploadDst,"wb")
            fileDescriptor.write(fileBuffer)
            fileDescriptor.close()
            
            #acknowledge written file output
            clientSocket.send("Saved file to: {0}\r\n".format(uploadDst).encode())
        except:
            clientSocket.send("Failed to save file to {0}.\r\n".format(uploadDst).encode())
            
    #check for command execution
    if execute is not None:
        print("Going to execute a command...")
        #run command
        output = runCommand(execute)
        clientSocket.send(output.encode())
        
    #go into another loop if command shell was requested
    if command:       
        print("Shell requested.") 
            #show a simple prompt
        clientSocket.send("<Pycat:#>".encode())
        while True:
                #now receive until we see a line feed
            cmdBuffer=""
            while "\n" not in cmdBuffer:
                cmdBuffer += clientSocket.recv(1024).decode()
                
            #send back command output
            response = runCommand(cmdBuffer)
            
            if isinstance(response,str):
                response = response.encode()
            
            #send back response
            clientSocket.send(response + "<Pycat:#>".encode())
    
    
def main():
    global listen
    global port
    global execute
    global command
    global uploadDst
    global target
    pycatDesc="""
    ---------------------------------------------------
            Pycat: A Python 3 Netcat Clone
    ---------------------------------------------------
    Example Usage:
    python3 pycat.py -t 192.168.1.1 4444 -l -c
    python3 pycat.py -t 192.168.1.1 4444 -l -u=C:\\test.exe
    python3 pycat.py -t 192.168.1.1 4444 -l -e=ls
              """
    
    parser = argparse.ArgumentParser(description=pycatDesc, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("port",type=int,help="This is the target port.")
    parser.add_argument("-t","--targetHost",type=str,help="This is the target host.",default="0.0.0.0")
    parser.add_argument("-l","--listen",help="listen on <host>:<port> for incoming connections.",action="store_true", default=False)
    parser.add_argument("-e","--execute",help="Execute a given file after receiving a connection.")
    parser.add_argument("-c","--command",help="Start a command shell", action="store_true",default=False)
    parser.add_argument("-u","--upload",help="After receiving a connection, upload a file and write it to [destination]")
    args = parser.parse_args()
    
    #parse arguments
    target = args.targetHost
    port = args.port
    listen = args.listen
    execute = args.execute
    command = args.command
    uploadDst = args.upload
                            
    #Listen or just send data from stdin?
    if not listen and target is not None and port > 0:
        print("Read data from stdin.")
        #read in buffer from cmd
        #this will block, send CTRL-D if not sending input
        buffer = sys.stdin.read()
        print("Sending{0} to client".format(buffer))
        #send data off
        clientSender(buffer)
    #Listen and potentially upload, execute, and a drop a shell
    if listen:
        serverLoop()        
main()
